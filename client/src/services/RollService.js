import Api from '@/services/Api';

export default {
  getRolls() {
    return Api().get('rolls');
  },

  createRoll(params) {
    return Api().post('rolls/create', params);
  },

  updateRoll(params) {
    return Api().post('rolls/update/' + params.id, params);
  },

  getRoll(params) {
    return Api().get('rolls/' + params.id, params);
  },

  deleteRoll(id) {
    return Api().get('rolls/delete/' + id);
  }
}
