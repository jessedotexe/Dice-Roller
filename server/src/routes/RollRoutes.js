const Roll = require('../models/Roll');

module.exports = {
    readRolls : function(req, res) {
        console.log("Read Rolls");
        Roll.find((err, rolls) => {
            if(err) {
                console.log(err);
            }
            else {
                res.json(rolls);
            }
        });
    },

    readRoll : function(req, res) {
        console.log("Read Roll");
        Roll.findById(req.params.id, (err, roll) => {
           if(err) {
               console.log(err);
           }
           else {
               console.log(roll);
               res.json(roll);
           }
        });
    },

    updateRoll : function(req, res) {
        console.log("Update Roll");
        Roll.findById(req.params.id, (err, roll) => {
            if(!roll) {
                return next(new Error('Could not find record.'));
            }
            else {
                roll.name = req.body.name;
                roll.number = req.body.number;
                roll.size = req.body.size;
                roll.modifier = req.body.modifier;

                roll.save()
                .then(roll => {
                   res.json('Update done.');
                })
                .catch(err => {
                   console.log(err);
                   res.status(400).send('Update failed.');
                });
            }
        });
    },

    createRoll : function(req, res) {
        console.log("Create Roll");
        let newRoll = new Roll();
        newRoll.name = req.body.name;
        newRoll.number = req.body.number;
        newRoll.size = req.body.size;
        newRoll.modifier = req.body.modifier;

        newRoll.save()
        .then(roll => {
            res.status(200).json({'roll': 'New record created.'});
        })
        .catch(err => {
            res.status(400).send('Failed to create new record.');
        });
    },

    deleteRoll : function(req, res) {
        console.log("Delete Roll");
        Roll.findByIdAndRemove({_id: req.params.id}, (err, roll) => {
           if(err) {
               res.json(err);
           }
           else {
               res.json('Delete successful.');
           }
        });
    }
};