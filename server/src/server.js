//Dependencies
import express from 'express';
import bodyParser from  'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';

//Routes
import RollRoutes from './routes/RollRoutes';

//Express Setup
const server = express();
const router = express.Router();
server.use(cors());
server.use(bodyParser.json());

//Mongoose Setup
mongoose.connect('mongodb://jesse:password1@ds053305.mlab.com:53305/dice');
const connection = mongoose.connection;

connection.on('error', console.error.bind(console, 'connection error'));
connection.once('open', () => {
    console.log('Connection Successful!');
});

//Routing
server.use('/', router);

router.route('/rolls').get(RollRoutes.readRolls);
router.route('/rolls/:id').get(RollRoutes.readRoll);
router.route('/rolls/update/:id').post(RollRoutes.updateRoll);
router.route('/rolls/create').post(RollRoutes.createRoll);
router.route('/rolls/delete/:id').get(RollRoutes.deleteRoll);

server.listen(8081, () => console.log('Express server running on port 8081.'));