import Api from '@/services/Api';

export default {
  fetchPosts() {
    return Api().get('posts');
  },

  addPost(params) {
    return Api().post('posts', params);
  },

  updatePost(params) {
    return Api().post('posts/edit/' + params.id, params);
  },

  getPost(params) {
    return Api().get('posts/' + params.id, params);
  },

  deletePost(id) {
    return Api().get('posts/delete/' + id);
  }
}
