var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RollSchema = new Schema({
    name: String,
    number: Number,
    size: Number,
    modifier: Number
});

const Roll = mongoose.model('Roll', RollSchema);

module.exports = Roll;